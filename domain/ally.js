/**
 * Ally Class
 *
 */
class Ally {
    #number;

    /**
     * Constructor
     * @param {*} number Allies quantity
     */
    constructor(number) {
      this.#number = number;
    }

    /**
     * Returns de number of allies
     */
    get number() {
      return this.#number;
    }
}

// Exporting Class
module.exports = Ally;
