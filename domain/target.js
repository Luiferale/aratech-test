/**
 * Target Class
 */
class Target {
    #coordinate;
    #enemies;
    #allies;

    /**
     * Constructor
     * @param {*} coordinate Target position
     * @param {*} enemies Enemies in target
     * @param {*} allies Allies in target
     */
    constructor(coordinate, enemies, allies) {
      this.#coordinate = coordinate;
      this.#enemies = enemies;
      this.#allies = allies;
    }

    /**
     * Returns target's coordinates
     */
    get coordinate() {
      return this.#coordinate;
    }

    /**
     * Returns enemies in target
     */
    get enemies() {
      return this.#enemies;
    }

    /**
     * Returns allies in target
     */
    get allies() {
      return this.#allies;
    }
}

// Exporting Class
module.exports = Target;
