/**
 * Vector2 class
 *
 * Design to handle coordinates behaviours
 */
class Vector2 {
    #x;
    #y;

    /**
     * Constructor
     * @param {*} x Axis X
     * @param {*} y Axis Y
     */
    constructor(x, y) {
      this.#x = x;
      this.#y = y;
    }

    /**
     * Returns Axis X value
     */
    get x() {
      return this.#x;
    }

    /**
     * Returns Axis Y value
     */
    get y() {
      return this.#y;
    }

    /**
     * Resolve the distance between two points
     * @param {Vector2} a Point A
     * @param {Vector2} b Point B
     * @return {Number}Returns the distance from A to B
     */
    static distance(a, b) {
      const x = a.x - b.x;
      const y = a.y - b.y;

      return Math.sqrt((Math.pow(x, 2)) + Math.pow(y, 2));
    }

    /**
     * Returns the origin point
     * @return {Vector2} Returns the origin
     */
    static zero() {
      return new Vector2(0, 0);
    }
}

// Exporting Class
module.exports = Vector2;
