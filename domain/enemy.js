/**
 * Enemy Class
 */
class Enemy {
    #type;
    #number;

    /**
     * Constructor
     * @param {*} type Enemy type {soldier, mech}
     * @param {*} number Enemies quantity
     */
    constructor(type, number) {
      this.#type = type;
      this.#number = number;
    }

    /**
     * Returns enemies type
     */
    get type() {
      return this.#type;
    }

    /**
     * Returns number of enemies
     */
    get number() {
      return this.#number;
    }
}

// Exporting Class
module.exports = Enemy;
