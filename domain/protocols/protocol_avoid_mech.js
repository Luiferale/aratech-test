// Dependencies
const Protocol = require('./protocol');

/**
 * Class AvoidMech extends Protocol
 *
 * A protocol focused on finding targets without mechs
 */
class AvoidMech extends Protocol {
  /**
     * Constructor
     */
  constructor() {
    super();
  }

  /**
     * Finds mech enemies and returns a filtered list of targets without them.
     * @param {[]} targets A list with targets
     * @return {[]} Returns the filtered list
     */
  getTarget(targets) {
    console.log('Avoid Mech');
    const arr = [];
    const arrNotMechs = [];

    targets.forEach((element) => {
      if (element.enemies.type === 'mech') {
        arr.push(element);
      } else {
        arrNotMechs.push(element);
      }
    });

    return arrNotMechs;
  }
}

// Exporting Class
module.exports = AvoidMech;
