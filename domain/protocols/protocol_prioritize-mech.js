// Dependencies
const Protocol = require('./protocol');

/**
 * Class PrioritizeMech extends Protocol
 *
 * A protocol focused on finding mechs
 */
class PrioritizeMech extends Protocol {
  /**
     * Constructor
     */
  constructor() {
    super();
  }

  /**
     * Finds mech enemies and returns a filtered list of mech
     * targets if possible.
     * @param {[]} targets A list with targets
     * @return {[]} Returns the filtered list
     */
  getTarget(targets) {
    console.log('Prioritize Mech');
    const arr = [];
    const arrNotMechs = [];

    targets.forEach((element) => {
      if (element.enemies.type === 'mech') {
        arr.push(element);
      } else {
        arrNotMechs.push(element);
      }
    });

    return arr.length > 0 ? arr : arrNotMechs;
  }
}

// Exporting Class
module.exports = PrioritizeMech;
