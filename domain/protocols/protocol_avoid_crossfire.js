// Dependencies
const Protocol = require('./protocol');

/**
 * Class AvoidCrossfire extends Protocol
 *
 * A protocol focused on finding targets without allies
 */
class AvoidCrossfire extends Protocol {
  /**
     * Constructor
     */
  constructor() {
    super();
  }

  /**
     * Finds allies and returns a filtered list of targets without allies.
     * @param {[]} targets A list with targets
     * @return {[]} Returns the filtered list
     */
  getTarget(targets) {
    console.log('Avoid Crossfire');
    const arr = [];
    const arrNotAllies = [];

    targets.forEach((element) => {
      if (element.allies != null) {
        arr.push(element);
      } else {
        arrNotAllies.push(element);
      }
    });

    return arrNotAllies;
  }
}

// Exporting Class
module.exports = AvoidCrossfire;
