// Dependencies
const Protocol = require('./protocol');

/**
 * Class AssistAllies extends Protocol
 *
 * A protocol focused on finding allies
 */
class AssistAllies extends Protocol {
  /**
     * Constructor
     */
  constructor() {
    super();
  }

  /**
     * Finds allies and returns a filtered list of targets
     * with allies if possible.
     * @param {[]} targets A list with targets
     * @return {[]} Returns the filtered list
     */
  getTarget(targets) {
    console.log('Assist Allies');
    const arr = [];
    const arrNotAllies = [];

    targets.forEach((element) => {
      if (element.allies != null) {
        arr.push(element);
      } else {
        arrNotAllies.push(element);
      }
    });

    return arr.length > 0 ? arr : arrNotAllies;
  }
}

// Exporting Class
module.exports = AssistAllies;
