// Dependencies
const ClosestEnemies = require('./protocol_closest_enemies');
const FurthestEnemies = require('./protocol_furthest_enemies');
const AssistAllies = require('./protocol_assist_allies');
const AvoidCrossfire = require('./protocol_avoid_crossfire');
const PrioritizeMech = require('./protocol_prioritize-mech');
const AvoidMech = require('./protocol_avoid_mech');

/**
 * ProtocolFactory class
 *
 * Pattern: Factory
 */
class ProtocolFactory {
  /**
     * Static function to retrive the protocol specified
     * @param {*} protocol Protocol to create
     * @return {Protocol} Return the specified protocol
     */
  static getProtocol(protocol) {
    let prot = null;
    switch (protocol) {
      case 'closest-enemies':
        prot = new ClosestEnemies();
        break;

      case 'furthest-enemies':
        prot = new FurthestEnemies();
        break;

      case 'assist-allies':
        prot = new AssistAllies();
        break;

      case 'avoid-crossfire':
        prot = new AvoidCrossfire();
        break;

      case 'prioritize-mech':
        prot = new PrioritizeMech();
        break;

      case 'avoid-mech':
        prot = new AvoidMech();
        break;

      default:
        prot = null;
        break;
    }

    return prot;
  }
}

// Exporting class
module.exports = ProtocolFactory;
