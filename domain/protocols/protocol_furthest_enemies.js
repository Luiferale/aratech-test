// Dependencies
const Protocol = require('./protocol');
const Vector2 = require('../vector2');

/**
 * Class FurthestEnemies extends Protocol
 *
 * A protocol focused on finding the furthest enemy
 */
class FurthestEnemies extends Protocol {
  /**
     * Constructor
     */
  constructor() {
    super();
  }

  /**
     * Finds the furthest targets and returns a sorted list.
     * @param {[]} targets A list with targets
     * @return {[]} Returns the sorted list
     */
  getTarget(targets) {
    console.log('Furthest Enemies');
    const arr = [];

    targets.forEach((element) => {
      if (arr.length > 0) {
        const dist = Vector2.distance(Vector2.zero(), element.coordinate);
        if (dist > Vector2.distance(Vector2.zero(), arr[0].coordinate)) {
          arr.unshift(element);
        } else {
          let isSet = false;
          let ele1 = element;
          let ele2;
          for (let i = 1; i < arr.length; i++) {
            if (dist > Vector2.distance(Vector2.zero(), arr[i].coordinate) &&
            !isSet) {
              ele1 = arr[i];
              arr[i] = element;
              isSet = true;
            } else if (isSet) {
              ele2 = arr[i];
              arr[i] = ele1;
              ele1 = ele2;
            }
          }
          arr.push(ele1);
        }
      } else {
        arr.push(element);
      }
    });

    return arr;
  }
}

// Exporting Class
module.exports = FurthestEnemies;
