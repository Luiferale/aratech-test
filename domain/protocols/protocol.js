/**
 * Protocol Abstract class
 */
class Protocol {
  /**
     * Constructor of Abstract Class.
     * This class can not be instantiated
     */
  constructor() {
    if (new.target === Protocol) {
      throw new Error('Abstract class can not be instanciated');
    }
  }

  /**
     * Abstract method.
     * @param {*} targets List of targes
     */
  getTarget(targets) {
    throw new Error('Abstract method requires to be implemented');
  }
}

// Exporting Class
module.exports = Protocol;
