/* Controllers */
const controllerRadar = require('../controller/controller_radar');

/* Router Handler */
module.exports = function name(router) {
  router.post('/radar', controllerRadar.getTarget);

  return router;
};
