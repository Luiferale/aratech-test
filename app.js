// Dependencies
const dotenv = require('dotenv');
const express = require('express');
const routes = require('./router/routes');

// Initialization
dotenv.config();
const app = express();
// eslint-disable-next-line new-cap
const router = express.Router();
const port = process.env.PORT;
const responseHandler = (req, res) => {
  const locals = req.res.locals;
  if (locals != null && '{}' != JSON.stringify(locals) &&
        hasOwnProperty.call(locals, 'status')) {
    res.status(locals.status).send(locals.response);
  } else {
    res.status(500).send({error: 'Error caught'});
  }
};

// Configuration
app.use(express.json());
app.use((err, req, res, next) => {
  if (err) {
    responseHandler(req, res);
  } else {
    next();
  }
});

// Routes Handler
app.use('/', routes(router));

// Response Handler
app.use(responseHandler);

// Start Server
app.listen(port, ()=>{
  console.log(`Server listening at http://localhost:${port}/`);
});
