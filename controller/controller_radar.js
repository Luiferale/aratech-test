// Dependencies
const ProtocolFactory = require('./../domain/protocols/protocol_factory');
const Vector2 = require('./../domain/vector2');
const Enemy = require('../domain/enemy');
const Ally = require('./../domain/ally');
const Target = require('./../domain/target');

/**
 * getTarget function to return the selected Target position
 * @param {*} req
 * @param {*} res
 */
getTarget = (req, res) => {
  const data = req.body;
  let status = 500;
  let response = {};

  if (data != null && '{}' != JSON.stringify(data)) {
    try {
      // Create Protocols
      const protocols = [];
      data.protocols.forEach((prot) => {
        const p = ProtocolFactory.getProtocol(prot);
        if ( p != null) {
          protocols.push(p);
        }
      });

      // Retrive targets
      let targets = [];
      data.scan.forEach((tg) => {
        const coords = new Vector2(tg.coordinates.x, tg.coordinates.y);
        const enems = new Enemy(tg.enemies.type, tg.enemies.number);
        let alls = null;
        if (Object.hasOwnProperty.call(tg, 'allies')) {
          alls = new Ally(tg.allies);
        }
        targets.push(new Target(coords, enems, alls));
      });

      // Get accurate target
      if (protocols.length > 0) {
        protocols.forEach((prot) => {
          targets = prot.getTarget(targets);
        });

        if (targets.length > 0) {
          let tg;
          let i = 0;
          // Get the acuurate target below max distance
          while (Vector2.distance(
              Vector2.zero(), targets[i].coordinate) > 100) {
            i++;
          }
          if (i < targets.length) {
            tg = targets[i];
            response.x = tg.coordinate.x;
            response.y = tg.coordinate.y;
          }
        }
        status = 200;
      }
    } catch (error) {
      // When error caught
      response = {error: 'Error caught'};
    }
  }

  res.locals['status'] = status;
  res.locals['response'] = response;
  req.next();
};

// Exporting Methods
module.exports = {
  getTarget,
};
